import { Entity } from "xiv-mech-sim/entities.js";
import { RoundArena } from "xiv-mech-sim/arena.js";
import { createSVGElement } from "xiv-mech-sim/drawing.js";
import { standardWarningColor } from "xiv-mech-sim/constants.js";
import { rotatePoint, distance } from "xiv-mech-sim/utilities.js";

class Arena extends RoundArena {
  #svgGroup;

  constructor(sim) {
    super(sim, 40, "#4a7595");
    this.#svgGroup = null;
  }

  addToDrawing(drawing) {
    super.addToDrawing(drawing);
    this.#svgGroup = createSVGElement("g");
    let linesOnTheFloor = createSVGElement("path");
    linesOnTheFloor.setAttribute(
      "d",
      "M100,80 L100,120 M80,100 L120,100 " +
        "M85.86,85.86 L114.14,114.14 M85.86,114.14 L114.14 85.86 " +
        "M100,92 a8 8 180 1 1 0,16 a8 8 180 1 1 0,-16 " +
        "M100,86 a14 14 180 1 1 0,28 a14 14 180 1 1 0,-28",
    );
    linesOnTheFloor.setAttribute("stroke-width", "0.1");
    linesOnTheFloor.setAttribute("stroke", "#bff4ff");
    linesOnTheFloor.setAttribute("fill", "none");
    this.#svgGroup.append(linesOnTheFloor);
    let pinkWallOfDeath = createSVGElement("circle");
    pinkWallOfDeath.setAttribute("cx", "100");
    pinkWallOfDeath.setAttribute("cy", "100");
    pinkWallOfDeath.setAttribute("r", "21");
    pinkWallOfDeath.setAttribute("stroke-width", "2");
    pinkWallOfDeath.setAttribute("stroke", "#c033f5");
    pinkWallOfDeath.setAttribute("fill", "none");
    this.#svgGroup.append(pinkWallOfDeath);
    let higherContrastDeathWallRing = pinkWallOfDeath.cloneNode();
    higherContrastDeathWallRing.setAttribute("r", "20.1");
    higherContrastDeathWallRing.setAttribute("stroke-width", "0.2");
    higherContrastDeathWallRing.setAttribute("stroke", "#ffefff");
    this.#svgGroup.append(higherContrastDeathWallRing);
    drawing.layers.arena.append(this.#svgGroup);
  }

  removeFromDrawing() {
    this.#svgGroup.remove();
    this.#svgGroup = null;
    super.removeFromDrawing();
  }
}

const IceWallPathSpec = "M92.35,81.52 a20 20 22.5 0 1 15.3,0 l0,1 -15.3,0 z";

class IceWall extends Entity {
  #direction;
  #path;
  has_hit;

  constructor(direction) {
    super();
    this.#direction = direction;
    this.has_hit = false;
  }

  addToDrawing(drawing) {
    this.#path = createSVGElement("path");
    this.#path.setAttribute("d", IceWallPathSpec);
    if (this.#direction != 0) {
      this.#path.setAttribute(
        "transform",
        `rotate(${this.#direction * 45} 100 100)`,
      );
    }
    if (this.has_hit) {
      this.#path.setAttribute("fill", "#9af1ff");
    } else {
      this.#path.setAttribute("fill", standardWarningColor);
    }
    drawing.layers.unclippedMechanics.append(this.#path);
  }

  removeFromDrawing() {
    this.#path.remove();
    this.#path = null;
  }

  hit(partyList) {
    for (let pm of partyList) {
      if (this.#containsPoint(pm.x, pm.y)) {
        pm.ko();
      }
    }
    this.#path.setAttribute("fill", "#9af1ff");
    this.has_hit = true;
  }

  allowMovement(from, to) {
    if (this.has_hit && this.#containsPoint(to.x, to.y)) {
      return false;
    } else {
      return true;
    }
  }

  #containsPoint(x, y) {
    // rotate point and wall around 100, 100 to put this wall north
    // (wall rotation not actually needed, will just compare to where
    // the wall would be if it were north)
    let rotated = rotatePoint({ x: x, y: y }, -45 * this.#direction);
    if (rotated.y < 82.52 && rotated.x > 92.35 && rotated.x < 107.65) {
      // don't care if it's actually past the wall of death
      return true;
    }
    return false;
  }
}

class IceWalls extends Entity {
  #walls;

  constructor(intercards) {
    super();
    let directions = intercards ? [1, 3, 5, 7] : [0, 2, 4, 6];
    this.#walls = [];
    for (let d of directions) {
      this.#walls.push(new IceWall(d));
    }
  }

  addToDrawing(drawing) {
    for (let w of this.#walls) {
      w.addToDrawing(drawing);
    }
  }

  removeFromDrawing() {
    for (let w of this.#walls) {
      w.removeFromDrawing();
    }
  }

  hit(partyList) {
    for (let w of this.#walls) {
      w.hit(partyList);
    }
  }

  allowMovement(from, to) {
    for (let w of this.#walls) {
      if (!w.allowMovement(from, to)) {
        return false;
      }
    }
    return true;
  }
}

const diagonalMovementVectorComponent = 1 / Math.sqrt(2);

const orbStartDirectionToMovementVectors = {
  0: { x: 0, y: 1 },
  1: {
    x: -1 * diagonalMovementVectorComponent,
    y: diagonalMovementVectorComponent,
  },
  2: { x: -1, y: 0 },
  3: {
    x: -1 * diagonalMovementVectorComponent,
    y: -1 * diagonalMovementVectorComponent,
  },
  4: { x: 0, y: -1 },
  5: {
    x: diagonalMovementVectorComponent,
    y: -1 * diagonalMovementVectorComponent,
  },
  6: { x: 1, y: 0 },
  7: { x: diagonalMovementVectorComponent, y: diagonalMovementVectorComponent },
};

const fireTravelTime = 8000;
const fissureWarningTime = 6620;
const fissureWarningDisappearsTime = 7500;
const fissureHitEffectDisappearsTime = 9000;

const longFissureLength = 17;
const shortFissureLength = 8;

class Fissure extends Entity {
  #svgGroup;
  #glowingLine;
  #fire;
  #bigCircle;
  #angle;
  #isLong;
  #running;
  #timeSoFar;
  #pathLength;

  constructor(degreesCWFromN, isLong) {
    super();
    this.#angle = degreesCWFromN;
    this.#isLong = isLong;
    this.#svgGroup = null;
    this.#glowingLine = null;
    this.#fire = null;
    this.#running = false;
    this.#timeSoFar = 0;
  }

  addToDrawing(drawing) {
    let transform = `rotate(${this.#angle},100,100)`;
    let pathSpec = `M100,100 l0,-${
      this.#isLong ? longFissureLength : shortFissureLength
    }`;
    this.#svgGroup = createSVGElement("g");
    this.#svgGroup.setAttribute("transform", transform);
    let baseLine = createSVGElement("path");
    baseLine.setAttribute("d", pathSpec);
    baseLine.setAttribute("stroke-width", "0.2");
    baseLine.setAttribute("stroke", "#111");
    baseLine.setAttribute("fill", "none");
    this.#pathLength = baseLine.getTotalLength();
    this.#svgGroup.append(baseLine);
    this.#glowingLine = baseLine.cloneNode();
    this.#glowingLine.setAttribute("stroke", "#fefe5d");
    this.#glowingLine.setAttribute("stroke-dasharray", `${this.#pathLength}`);
    this.#glowingLine.setAttribute("stroke-dashoffset", `-${this.#pathLength}`);
    this.#svgGroup.append(this.#glowingLine);
    drawing.layers.unclippedMechanics.append(this.#svgGroup);
  }

  removeFromDrawing() {
    if (this.#svgGroup) {
      this.#svgGroup.remove();
      this.#svgGroup = null;
      this.#glowingLine = null;
      this.#fire = null;
      this.#bigCircle = null;
    }
  }

  // must not be called before addToDrawing
  start(timeToJumpTo) {
    this.#fire = createSVGElement("circle");
    this.#fire.setAttribute("cx", "100");
    this.#fire.setAttribute("cy", "100");
    this.#fire.setAttribute("r", "0.15");
    this.#fire.setAttribute("fill", "#fffde3");
    this.#svgGroup.append(this.#fire);
    this.#timeSoFar = timeToJumpTo;
    this.#running = true;
  }

  update(sim, msSinceLastUpdate) {
    if (this.#running) {
      this.#timeSoFar += msSinceLastUpdate;
      if (this.#timeSoFar < fireTravelTime) {
        let amountBurning = this.#timeSoFar / fireTravelTime;
        this.#glowingLine.setAttribute(
          "stroke-dashoffset",
          `${(1 - amountBurning) * this.#pathLength}`,
        );
        let firePoint = this.#glowingLine.getPointAtLength(
          amountBurning * this.#pathLength,
        );
        this.#fire.setAttribute("cx", `${firePoint.x}`);
        this.#fire.setAttribute("cy", `${firePoint.y}`);
      } else {
        if (this.#glowingLine) {
          this.#glowingLine.remove();
          this.#glowingLine = null;
          this.#fire.remove();
          this.#fire = null;
        }
      }
      if (
        this.#timeSoFar >= fissureWarningTime &&
        this.#timeSoFar < fissureWarningDisappearsTime &&
        this.#bigCircle == null
      ) {
        this.#bigCircle = createSVGElement("circle");
        this.#bigCircle.setAttribute("cx", "100");
        this.#bigCircle.setAttribute(
          "cy",
          `${100 - (this.#isLong ? longFissureLength : shortFissureLength)}`,
        );
        this.#bigCircle.setAttribute("r", "8");
        this.#bigCircle.setAttribute("fill", standardWarningColor);
        this.#svgGroup.append(this.#bigCircle);
      } else if (
        this.#timeSoFar >= fissureWarningDisappearsTime &&
        this.#timeSoFar < fireTravelTime
      ) {
        if (this.#bigCircle) {
          this.#bigCircle.remove();
          // keeping to recycle for hit, will probably be fine

          // this is when we ko people
          let endPoint = rotatePoint(
            {
              x: 100,
              y: 100 - (this.#isLong ? longFissureLength : shortFissureLength),
            },
            this.#angle,
          );
          for (let pm of sim.party.list) {
            if (distance(endPoint, pm) <= 8) {
              pm.ko();
            }
          }
        }
      } else if (
        this.#timeSoFar >= fireTravelTime &&
        this.#timeSoFar < fissureHitEffectDisappearsTime
      ) {
        this.#bigCircle.setAttribute("fill", "rgba(140,40,0,0.8)");
        this.#svgGroup.append(this.#bigCircle);
      } else if (this.#timeSoFar >= fissureHitEffectDisappearsTime) {
        if (this.#bigCircle) {
          this.#bigCircle.remove();
          this.#bigCircle = null;
        }
      }
    }
  }
}

class Fissures extends Entity {
  #firstFissures;
  #secondFissures;

  constructor(outerNWSESafeFirst) {
    super();
    let cardinalFissures = [];
    cardinalFissures.push(new Fissure(0, true));
    cardinalFissures.push(new Fissure(90, true));
    cardinalFissures.push(new Fissure(180, true));
    cardinalFissures.push(new Fissure(270, true));
    let outerNESWSet = [];
    outerNESWSet.push(new Fissure(45, true));
    outerNESWSet.push(new Fissure(112.5, false));
    outerNESWSet.push(new Fissure(225, true));
    outerNESWSet.push(new Fissure(292.5, false));
    let outerNWSESet = [];
    outerNWSESet.push(new Fissure(22.5, false));
    outerNWSESet.push(new Fissure(135, true));
    outerNWSESet.push(new Fissure(202.5, false));
    outerNWSESet.push(new Fissure(315, true));
    if (outerNWSESafeFirst) {
      this.#firstFissures = cardinalFissures.concat(outerNESWSet);
      this.#secondFissures = outerNWSESet;
    } else {
      this.#firstFissures = cardinalFissures.concat(outerNWSESet);
      this.#secondFissures = outerNESWSet;
    }
  }

  addToDrawing(drawing) {
    for (let fissure of this.#firstFissures.concat(this.#secondFissures)) {
      fissure.addToDrawing(drawing);
    }
  }

  removeFromDrawing() {
    for (let fissure of this.#firstFissures.concat(this.#secondFissures)) {
      fissure.removeFromDrawing();
    }
  }

  update(sim, msSinceLastUpdate) {
    for (let fissure of this.#firstFissures.concat(this.#secondFissures)) {
      fissure.update(sim, msSinceLastUpdate);
    }
  }

  startFirstSet(timeAtStart = 0) {
    for (let fissure of this.#firstFissures) {
      fissure.start(timeAtStart);
    }
  }

  startSecondSet(timeAtStart = 0) {
    for (let fissure of this.#secondFissures) {
      fissure.start(timeAtStart);
    }
  }
}

class SwingingKick extends Entity {
  #svgGroup;

  constructor(isFront) {
    super();
    this.isFront = isFront;
    this.#svgGroup = null;
  }

  addToDrawing(drawing) {
    this.#svgGroup = createSVGElement("g");
    drawing.layers.arenaClippedMechanics.append(this.#svgGroup);
  }

  removeFromDrawing() {
    if (this.#svgGroup) {
      this.#svgGroup.remove();
      this.#svgGroup = null;
    }
  }

  hit(partyList) {
    let rect = createSVGElement("rect");
    rect.setAttribute("x", "80");
    rect.setAttribute("y", this.isFront ? "80" : "100");
    rect.setAttribute("width", "40");
    rect.setAttribute("height", "20");
    rect.setAttribute("fill", "rgba(140,40,0,0.8)");
    this.#svgGroup.append(rect);

    for (let pm of partyList) {
      if ((this.isFront && pm.y <= 100) || (!this.isFront && pm.y >= 100)) {
        pm.ko();
      }
    }
  }
}

export { Arena, IceWalls, Fissures, SwingingKick };
