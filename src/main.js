import { Sim } from "xiv-mech-sim";
import { Hitbox, Castbar, Success } from "xiv-mech-sim/info_displays.js";
import {
  CircleAoE,
  DonutAoE,
  LPStacks,
  Enums,
} from "xiv-mech-sim/mechanics.js";
import { rotatePoint, distance } from "xiv-mech-sim/utilities.js";
import { Arena, IceWalls, Fissures, SwingingKick } from "./mechanics.js";

let sim = new Sim({
  placeholderId: "sim",
  drawingSize: 50, // yalms!
  backgroundColor: "rgb(0,0,0)",
  menuId: "menu",
  initFn: function (sim) {
    rollRNG(sim);
    sim.entities.add(new Arena(sim));
    setUpMechanics(sim);
    setUpDutySupport(sim);
  },
});

sim.run();

let rngChoices = {};

function rollRNG(sim) {
  rngChoices.outerNWSESafeFirst = Math.random() > 0.5;
  rngChoices.frontCombination = Math.random() > 0.5;
  rngChoices.donutFirst = Math.random() > 0.5;
  rngChoices.iceWallsIntercardinal = Math.random() > 0.5;
  console.log(JSON.stringify(rngChoices));
}

// all times are in milliseconds
const buddyStacksHit = 700;
const firstFissuresTimeAtZero = 640;
const combinationCastbarAppears = 380;
const combinationCastbarLength = 5700;
const secondFissuresStart = 5360;
const inOutWarning = combinationCastbarAppears;
const inOutHitOne = 6730;
const kickHit = 10440;
const inOutHitTwo = 12730;
const archaicDemolishCastbarAppears = 15730;
const archaicDemolishCastbarDuration = 3700;
const archaicDemolishHit =
  archaicDemolishCastbarAppears + archaicDemolishCastbarDuration;

const succeed = archaicDemolishHit + 1000;

const hitEffectDuration = 1000;

function setUpMechanics(sim) {
  sim.entities.add(new Hitbox(19));
  let iceWalls = new IceWalls(rngChoices.iceWallsIntercardinal);
  sim.entities.add(iceWalls);
  iceWalls.hit([]);

  let buddyStacks = new Enums(sim, true); // close enough
  sim.timeline.addEvent(buddyStacksHit, () => {
    sim.entities.add(buddyStacks);
    buddyStacks.hit(sim.party.list);
  });
  sim.timeline.addEvent(buddyStacksHit + hitEffectDuration, () => {
    sim.entities.remove(buddyStacks);
  });

  let fissures = new Fissures(rngChoices.outerNWSESafeFirst);
  sim.entities.add(fissures);
  fissures.startFirstSet(firstFissuresTimeAtZero);
  sim.timeline.addEvent(secondFissuresStart, () => {
    fissures.startSecondSet();
  });

  let castbar = new Castbar(
    rngChoices.frontCombination ? "Front Combination" : "Rear Combination",
    combinationCastbarLength,
  );
  let kick = new SwingingKick(rngChoices.frontCombination);
  sim.timeline.addEvent(combinationCastbarAppears, () => {
    sim.entities.add(castbar);
  });
  sim.timeline.addEvent(
    combinationCastbarAppears + combinationCastbarLength,
    () => {
      sim.entities.remove(castbar);
    },
  );
  sim.timeline.addEvent(kickHit, () => {
    sim.entities.add(kick);
    kick.hit(sim.party.list);
  });
  sim.timeline.addEvent(kickHit + hitEffectDuration, () => {
    sim.entities.remove(kick);
  });

  let pointBlank = new CircleAoE({
    target: { x: 100, y: 100 },
    radius: 12,
    hit_color: "rgba(140,40,0,0.5)",
  });
  let donut = new DonutAoE({
    target: { x: 100, y: 100 },
    inner_radius: 8,
    outer_radius: 20,
    hit_color: "rgba(140,40,0,0.5)",
  });
  let firstInOut = rngChoices.donutFirst ? donut : pointBlank;
  let secondInOut = rngChoices.donutFirst ? pointBlank : donut;
  sim.timeline.addEvent(inOutWarning, () => {
    sim.entities.add(firstInOut);
  });
  sim.timeline.addEvent(inOutHitOne, () => {
    firstInOut.hit(sim.party.list);
  });
  sim.timeline.addEvent(inOutHitOne + hitEffectDuration, () => {
    sim.entities.remove(firstInOut);
  });
  sim.timeline.addEvent(inOutHitTwo, () => {
    sim.entities.add(secondInOut);
    secondInOut.hit(sim.party.list);
  });
  sim.timeline.addEvent(inOutHitTwo + hitEffectDuration, () => {
    sim.entities.remove(secondInOut);
  });

  let demolishCastbar = new Castbar(
    "Archaic Demolish",
    archaicDemolishCastbarDuration,
  );
  let demolishStacks = new LPStacks(sim);
  sim.timeline.addEvent(archaicDemolishCastbarAppears, () => {
    sim.entities.add(demolishCastbar);
  });
  sim.timeline.addEvent(
    archaicDemolishCastbarAppears + archaicDemolishCastbarDuration,
    () => {
      sim.entities.remove(demolishCastbar);
    },
  );
  sim.timeline.addEvent(archaicDemolishHit, () => {
    sim.entities.add(demolishStacks);
    demolishStacks.hit(sim.party.list);
  });
  sim.timeline.addEvent(archaicDemolishHit + hitEffectDuration, () => {
    sim.entities.remove(demolishStacks);
  });

  sim.timeline.addEvent(succeed, () => {
    sim.entities.add(new Success());
    sim.endRun();
  });
}

const thinking_delay = 1500;
const reaction_delay = 500;

// make party slower to start moving so it's harder to just follow them
const bonus_delay_one = 500; // going to first safe spot
const bonus_delay_two = 500; // going to second safe spot
const bonus_delay_three = 1000; // going to stacks

function setUpDutySupport(sim) {
  let dutySupport = document.querySelector('[name="duty-support"]').checked;
  if (!dutySupport) {
    sim.party.hideDutySupport();
  }

  let initialAdjustment = rngChoices.iceWallsIntercardinal ? 45 : 0;
  let initialSpotOne = { x: 99.5, y: 83 };
  let initialSpotTwo = { x: 100.5, y: 83 };

  // we start after the knockback & partner stack
  let p = rotatePoint(initialSpotOne, initialAdjustment);
  sim.party.T1.set_position(p.x, p.y);
  p = rotatePoint(initialSpotTwo, initialAdjustment);
  sim.party.R2.set_position(p.x, p.y);
  p = rotatePoint(initialSpotOne, 90 + initialAdjustment);
  sim.party.H2.set_position(p.x, p.y);
  p = rotatePoint(initialSpotTwo, 90 + initialAdjustment);
  sim.party.M2.set_position(p.x, p.y);
  p = rotatePoint(initialSpotOne, 180 + initialAdjustment);
  sim.party.T2.set_position(p.x, p.y);
  p = rotatePoint(initialSpotTwo, 180 + initialAdjustment);
  sim.party.M1.set_position(p.x, p.y);
  p = rotatePoint(initialSpotOne, 270 + initialAdjustment);
  sim.party.H1.set_position(p.x, p.y);
  p = rotatePoint(initialSpotTwo, 270 + initialAdjustment);
  sim.party.R1.set_position(p.x, p.y);

  let safeSpotOne = {};
  if (rngChoices.donutFirst) {
    safeSpotOne = { x: 94, y: 98 };
    if (rngChoices.frontCombination) {
      safeSpotOne = rotatePoint(safeSpotOne, 180);
    }
    if (rngChoices.outerNWSESafeFirst) {
      safeSpotOne = rotatePoint(safeSpotOne, 90);
    }
  } else {
    safeSpotOne = { x: 113, y: 90 };
    if (rngChoices.frontCombination) {
      safeSpotOne = rotatePoint(safeSpotOne, 180);
    }
    if (rngChoices.outerNWSESafeFirst) {
      safeSpotOne = rotatePoint(safeSpotOne, -90);
    }
  }
  let alternateSafeSpotOne = rotatePoint(safeSpotOne, 180);

  let inAlternateSafeSpot = [];
  sim.timeline.addEvent(
    combinationCastbarAppears + thinking_delay + bonus_delay_one,
    () => {
      for (let pm of sim.party.list) {
        if (
          distance(pm, safeSpotOne) - distance(pm, alternateSafeSpotOne) <
          15
        ) {
          pm.set_target_positions([
            { x: blur(safeSpotOne.x, 1), y: blur(safeSpotOne.y, 1) },
          ]);
        } else {
          pm.set_target_positions([
            {
              x: blur(alternateSafeSpotOne.x, 1),
              y: blur(alternateSafeSpotOne.y, 1),
            },
          ]);
          inAlternateSafeSpot.push(pm);
        }
      }
    },
  );

  let safeSpotTwo = {};
  if (rngChoices.donutFirst) {
    safeSpotTwo = { x: 114, y: 96 };
    if (!rngChoices.outerNWSESafeFirst) {
      safeSpotTwo = rotatePoint(safeSpotTwo, -90);
    }
    if (rngChoices.frontCombination) {
      safeSpotTwo = rotatePoint(safeSpotTwo, 180);
    }
  } else {
    safeSpotTwo = { x: 102, y: 94 };
    if (rngChoices.outerNWSESafeFirst) {
      safeSpotTwo = rotatePoint(safeSpotTwo, -90);
    }
    if (rngChoices.frontCombination) {
      safeSpotTwo = rotatePoint(safeSpotTwo, 180);
    }
  }

  sim.timeline.addEvent(inOutHitOne + reaction_delay + bonus_delay_two, () => {
    for (let pm of sim.party.list) {
      if (inAlternateSafeSpot.includes(pm)) {
        if (pm.y > 100) {
          pm.set_target_positions([{ x: pm.x, y: 99 }]);
        } else {
          pm.set_target_positions([{ x: pm.x, y: 101 }]);
        }
        pm.add_target_position(blur(safeSpotTwo.x, 1), blur(safeSpotTwo.y, 1));
      } else {
        pm.set_target_positions([
          { x: blur(safeSpotTwo.x, 1), y: blur(safeSpotTwo.y, 1) },
        ]);
      }
    }
  });

  sim.timeline.addEvent(
    inOutHitTwo + reaction_delay + bonus_delay_three,
    () => {
      for (let pm of sim.party.list) {
        pm.set_target_positions([
          { x: blur(pm.lp == 1 ? 93 : 107, 1), y: blur(100, 1) },
        ]);
      }
      if (!dutySupport) {
        sim.party.showDutySupport();
      }
    },
  );
}

function blur(number, max_amount) {
  return number - max_amount + 2 * max_amount * Math.random();
}
